// navigator/MainNavigator.js
import React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import Landing from '../Components/LandingPage/Landing';
import Continue from '../Components/ContinuePage/Continue';
import Login from '../Components/LoginPage/Login';
import Register from '../Components/RegisterPage/Register';
import Main from '../Components/MainPage/Main';
import Menu from '../Components/MenuPage/Menu';
import Profile from '../Components/ProfilePage/Profile'
import Account from '../Components/AccountPage/Account'
import Setting from '../Components/SettingsPage/Setting'


const Stack = createNativeStackNavigator();

const MainNavigator = () => {
  return (
    <NavigationContainer>
      <Stack.Navigator
        screenOptions={{
          headerShown: false,
        }}>
        <Stack.Screen name="landing" component={Landing} />
        <Stack.Screen name="continue" component={Continue} />
        <Stack.Screen name="login" component={Login} />
        <Stack.Screen name="register" component={Register} />
        <Stack.Screen name="main" component={Main} />
        <Stack.Screen name="menu" component={Menu} />
        <Stack.Screen name="profile" component={Profile} />
        <Stack.Screen name="account" component={Account} />
        <Stack.Screen name="setting" component={Setting} />
      </Stack.Navigator>
    </NavigationContainer>
  );
};

export default MainNavigator;