// reduxSetup.js
import React from 'react';
import { Provider } from 'react-redux';
import { createStore, combineReducers } from 'redux';
import authReducer from '../Reducer/authReducer'; // Adjust the path based on your project structure

const rootReducer = combineReducers({
  auth: authReducer,
});

const store = createStore(rootReducer);

const ReduxSetup = ({ children }) => {
  return <Provider store={store}>{children}</Provider>;
};

export default ReduxSetup;
