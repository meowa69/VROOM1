// store/index.js
import { createStore, combineReducers } from 'redux';
import authReducer from '../Reducer/authReducer';

const rootReducer = combineReducers({
  auth: authReducer,
  // Add other reducers if needed
});

const store = createStore(rootReducer);

export default store;
