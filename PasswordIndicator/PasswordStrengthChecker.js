// PasswordStrengthChecker.js
import React from 'react';
import { View, Text } from 'react-native';

const PasswordStrengthChecker = ({ password, confirmPassword }) => {
  const calculateStrength = (password) => {
    if (password.length === 0) {
      return null; // Do not display strength indicator if password is empty
    } else if (password.length < 6) {
      return { strength: 'Weak', color: 'red' };
    } else if (password.length < 10) {
      return { strength: 'Moderate', color: 'orange' };
    } else {
      return { strength: 'Strong', color: 'green' };
    }
  };

  const strengthInfo = calculateStrength(password);

  if (!strengthInfo) {
    return null; // Do not display anything if strengthInfo is null
  }

  const { strength, color } = strengthInfo;

  // Check if the passwords match
  const passwordMatch = confirmPassword && password === confirmPassword;

  return (
    <View style={{ alignItems: 'flex-end', marginTop: -10, marginBottom: 5, marginRight: 10, width: 350 }}>
      <Text style={{ color }}>{strength} Password</Text>

      <View style={{alignSelf: 'center'}}>
        {!passwordMatch && confirmPassword ? (
          <Text style={{ color: 'red' }}>Password not match</Text>
        ) : null}
      </View>
    </View>
    
  );
};

export default PasswordStrengthChecker;
