export const login = (userData) => {
  return {
    type: 'LOGIN',
    payload: userData,
  };
};

export const register = (userData) => {
  return {
    type: 'REGISTER',
    payload: userData,
  };
};

export const logout = () => {
  return {
    type: 'LOGOUT',
  };
};

export const setProfileImage = (imageUri) => {
  return {
    type: 'SET_PROFILE_IMAGE',
    payload: imageUri,
  };
};