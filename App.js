import React from 'react';
import ReduxSetup from './ReduxSetup/ReduxSetup'; 
import MainNavigator from './Navigator/MainNavigator';


const App = () => {
  return (
    <ReduxSetup>
      <MainNavigator />
    </ReduxSetup>
  );
};

export default App;
