//account
import React from 'react';
import { View, Text, Image, StyleSheet, TouchableOpacity, ScrollView } from 'react-native';
import { useFonts } from 'expo-font';
import { useNavigation } from '@react-navigation/native';
import { useSelector } from 'react-redux';
import * as ImagePicker from 'expo-image-picker';
import { useFocusEffect } from '@react-navigation/native';
import { setProfileImage } from '../../Actions/authActions';
import { useDispatch } from 'react-redux';
import { connect } from 'react-redux';  // Add this import at the beginning of your file


const mapStateToProps = (state) => {
  return {
    // Map the state properties you need to props here
    registeredName: state.auth.registeredName,
    registeredEmail: state.auth.registeredEmail,
    registeredAddress: state.auth.registeredAddress,
    registeredUserType: state.auth.registeredUserType,
    registeredPassword: state.auth.registeredPassword,
    profileImage: state.auth.profileImage,
  };
};

// Define mapDispatchToProps outside the component
const mapDispatchToProps = (dispatch) => {
  return {
    // Map the action creators you need to props here
    setProfileImage: (imageUri) => dispatch(setProfileImage(imageUri)),
    // Add other action creators as needed
  };
};

const Account = (props) => {
  const dispatch = useDispatch();
  const navigation = useNavigation();
  const registeredName = useSelector((state) => state.auth.registeredName);
  const registeredEmail = useSelector((state) => state.auth.registeredEmail);
  const registeredAddress = useSelector((state) => state.auth.registeredAddress);
  const registeredUserType = useSelector((state) => state.auth.registeredUserType);
  const registeredPassword = useSelector((state) => state.auth.registeredPassword);
  const profileImage = useSelector((state) => state.auth.profileImage);


  const [fontsLoaded] = useFonts({
    'BalooChettan2-ExtraBold': require('../../assets/Fonts/BalooChettan2-ExtraBold.ttf'),
    'Poppins-Medium': require('../../assets/Fonts/Poppins-Medium.ttf'),
    'Poppins-SemiBold': require('../../assets/Fonts/Poppins-SemiBold.ttf'),
    'Poppins-Bold': require('../../assets/Fonts/Poppins-Bold.ttf'),
  });

  useFocusEffect(
    React.useCallback(() => {
      // Fetch or dispatch actions when the screen is focused
    }, [])
  );

  const handleMenuClose = () => {
    navigation.navigate('main');
  };

  const handleMenuBack = () => {
    navigation.goBack();
  };

  const handleEditProfilePicture = async () => {
    try {
      const result = await ImagePicker.launchImageLibraryAsync({
        mediaTypes: ImagePicker.MediaTypeOptions.Images,
        allowsEditing: true,
        aspect: [1, 1],
        quality: 1,
      });

      if (!result.cancelled) {
        const imageUri = result.uri || (result.assets && result.assets[0]?.uri);

        if (imageUri) {
          // Dispatch the action to set the profile image
          dispatch(setProfileImage(imageUri));
          console.log('Profile Image URI:', imageUri);
        } else {
          console.log('Image URI is undefined.');
        }
      } else {
        console.log('Image selection cancelled.');
      }
    } catch (error) {
      console.error('Error picking an image', error);
    }
  };


  return (
    <View style={styles.container}>
      <View style={styles.above}>
        <TouchableOpacity style={styles.closeBtn} onPress={handleMenuBack}>
          <Image source={require('../../assets/Images/left.png')} style={styles.close} />
        </TouchableOpacity>

        <View style={styles.logoname}>
          <Text style={styles.text}>vro</Text>
          <Image source={require('../../assets/Images/wheel.png')} style={styles.logo} />
          <Text style={styles.text}>m</Text>
        </View>

        <TouchableOpacity style={styles.closeBtn} onPress={handleMenuClose}>
          <Image source={require('../../assets/Images/close2.png')} style={styles.close} />
        </TouchableOpacity>
      </View>

      <ScrollView style={styles.scrollContainer}>
        <View style={styles.content}>
          <TouchableOpacity style={styles.editBtn} onPress={handleEditProfilePicture}>
            <Image source={require('../../assets/Images/pencil.png')} style={styles.editIcon} />
          </TouchableOpacity>

          {profileImage ? (
            <Image source={{ uri: profileImage }} style={styles.profile} />
          ) : (
            <View style={styles.profile}></View>
          )}
        </View>

        {/* details */}
            <View style={styles.detailsCon}>
                <Text style={styles.detailText1}>Account details</Text>
                <View style={styles.detailsContent}>
                <View style={styles.details}>
                    <View>
                    <Text style={styles.detailText2}>Full name</Text>
                    <Text style={styles.detailText}>{registeredName || ''}</Text>
                    </View>
                    
                    <TouchableOpacity style={styles.detailsEditCon}>
                    <Text style={styles.editText}>Edit</Text>
                    </TouchableOpacity>
                </View>

                <View style={styles.lineCon}>
                    <View style={styles.VerticalLine}/>
                </View>

                <View style={styles.details}>
                    <View>
                    <Text style={styles.detailText3}>User type</Text>
                    <Text style={styles.detailText}>{registeredUserType || ''}</Text>
                    </View>

                    <TouchableOpacity style={styles.detailsEditCon}>
                    <Text style={styles.editText}>Edit</Text>
                    </TouchableOpacity>
                </View>

                
                <View style={styles.lineCon}>
                    <View style={styles.VerticalLine}/>
                </View>

                <View style={styles.details}>
                    <View>
                    <Text style={styles.detailText3}>Address</Text>
                    <Text style={styles.detailText}>{registeredAddress || ''}</Text>
                    </View>

                    <TouchableOpacity style={styles.detailsEditCon}>
                    <Text style={styles.editText}>Edit</Text>
                    </TouchableOpacity>
                </View>
                </View>    
            </View>

            {/* login */}
            <View style={styles.LoginCon}>
                <Text style={styles.loginText1}>Login info</Text>
                <View style={styles.loginContent}>
                <View style={styles.details}>
                    <View>
                    <Text style={styles.loginText2}>Email</Text>
                    <Text style={styles.loginText}>{registeredEmail || ''}</Text>
                    </View>
                    
                    <TouchableOpacity style={styles.loginEditCon}>
                    <Text style={styles.editText}>Edit</Text>
                    </TouchableOpacity>
                </View>

                <View style={styles.lineCon}>
                    <View style={styles.VerticalLine}/>
                </View>

                <View style={styles.details}>
                    <View>
                    <Text style={styles.loginText3}>Password</Text>
                    <Text style={styles.loginText}>{registeredPassword || ''}</Text>
                    </View>

                    <TouchableOpacity style={styles.loginEditCon}>
                    <Text style={styles.editText}>Edit</Text>
                    </TouchableOpacity>
                </View>
                </View>    
            </View>
  
        </ScrollView>
      
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
  },
  scrollContainer:{
    flexGrow: 1,
  },

  above: {
    justifyContent: 'space-between',
    alignItems: 'flex-start',
    flexDirection: 'row',
    marginTop: 40,
  },
  logoname: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  text: {
    fontSize: 32,
    fontFamily: 'BalooChettan2-ExtraBold',
    color: '#67A1FF',
  },
  logo: {
    width: 20,
    height: 20,
    marginTop: 7,
    marginLeft: -2,
  },

  closeBtn: {
    paddingTop: 20,
    marginRight: 20,
    marginLeft: 20,
  },

  close: {
    width: 18,
    height: 18,
  },

  content: {
    width: '100%',
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 20,
    marginTop: 25,
  },

  profile: {
    width: 97,
    height: 93,
    backgroundColor: '#FFFCFC',
    borderWidth: 1,
    borderColor: '#BBBBBB',
    borderRadius: 100,
  },

  editIcon:{
    width: 13,
    height: 13,
  },

  editBtn:{
    position: 'absolute',
    bottom: -1,
    right: 165,
    width: 25,
    height: 25,
    backgroundColor: '#67A1FF',
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 50,
    zIndex: 1,
  },

  detailsCon:{
    marginTop: 40,
  },


detailText1:{
    fontSize: 16,
    fontFamily: 'Poppins-Medium',
    paddingLeft: 25,
    color: '#524F4F',
},

detailText2:{
    fontSize: 16,
    fontFamily: 'Poppins-Bold',
    color: '#fff',
    paddingTop: 10,
},

detailText3:{
    fontSize: 16,
    fontFamily: 'Poppins-Bold',
    color: '#fff',
},

detailText:{
    fontSize: 13,
    fontFamily: 'Poppins-Medium',
    color: '#fff',
},
  
details:{
    paddingLeft: 25,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
},

detailsEditCon:{
    width: 55,
    height: 25,
    marginRight: 25,
    backgroundColor: '#fff',
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 5,
},

editText:{
    fontSize: 13,
    fontFamily: 'Poppins-Medium',
    color: '#67A1FF',
},

detailsContent:{
    width: 414,
    height: 233,
    backgroundColor: '#67A1FF',
},

VerticalLine: {
    flex: 1,
    height: 2,
    backgroundColor: '#fff',
},

lineCon: {
    flexDirection: 'row',
    alignItems: 'center',
    marginVertical: 15,
    paddingLeft: 15,
    paddingRight: 15,
},

// login
loginContent:{
    width: 414,
    height: 153,
    backgroundColor: '#67A1FF',
},
LoginCon:{
    marginTop: 40,
  },

loginText:{
    fontSize: 13,
    fontFamily: 'Poppins-Medium',
    color: '#fff',
},

loginText1:{
    fontSize: 16,
    fontFamily: 'Poppins-Medium',
    paddingLeft: 25,
    color: '#524F4F',
},

loginText2:{
    fontSize: 16,
    fontFamily: 'Poppins-Bold',
    color: '#fff',
    paddingTop: 10,
},

loginText3:{
    fontSize: 16,
    fontFamily: 'Poppins-Bold',
    color: '#fff',
},

loginEditCon:{
    width: 55,
    height: 25,
    marginRight: 25,
    backgroundColor: '#fff',
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 5,
},

// advance
advanceContent:{
    width: 414,
    height: 180,
    backgroundColor: '#67A1FF',
    marginBottom: 20,
},
advanceCon:{
    marginTop: 40,
  },

advanceText:{
    fontSize: 16,
    fontFamily: 'Poppins-Medium',
    paddingLeft: 25,
    color: '#524F4F',
},
advanceText1:{
    fontSize: 16,
    fontFamily: 'Poppins-Bold',
    color: '#fff',
    paddingTop: 10,
},

advanceText2:{
    fontSize: 16,
    fontFamily: 'Poppins-Bold',
    color: '#fff',
    paddingTop: 10,
},

advanceText3:{
    fontSize: 16,
    fontFamily: 'Poppins-Bold',
    color: '#C86B6B',
},

advanceEditCon:{
    width: 55,
    height: 25,
    marginRight: 25,
    backgroundColor: '#fff',
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 5,
},
});

export default Account;