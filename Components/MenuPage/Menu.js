import React, { useEffect } from 'react';
import { View, Text, Image, StyleSheet, TouchableOpacity, BackHandler } from 'react-native';
import { useFonts } from 'expo-font';
import { useNavigation } from '@react-navigation/native';
import { useSelector } from 'react-redux';

const Menu = () => {
  const navigation = useNavigation();
  const registeredName = useSelector((state) => state.auth.registeredName);
  const profileImage = useSelector((state) => state.auth.profileImage);

  const [fontsLoaded] = useFonts({
    'BalooChettan2-ExtraBold': require('../../assets/Fonts/BalooChettan2-ExtraBold.ttf'),
    'Poppins-Medium': require('../../assets/Fonts/Poppins-Medium.ttf'),
    'Poppins-SemiBold': require('../../assets/Fonts/Poppins-SemiBold.ttf'),
    'Poppins-Bold': require('../../assets/Fonts/Poppins-Bold.ttf'),
  });

  useEffect(() => {
    const backHandler = BackHandler.addEventListener('hardwareBackPress', handleBackPress);

    return () => backHandler.remove();
  }, []);

  const handleBackPress = () => {
    BackHandler.exitApp();
    return true;
  };

  const handleMenuClose = () => {
    navigation.goBack();
  };

  const handleViewProfile = () => {
    navigation.navigate('profile');
  };

  const handleSetting = () => {
    navigation.navigate('setting');
  };

  return (
    <View style={styles.container}>
      <View style={styles.above}>
        <View style={styles.logoname}>
          <Text style={styles.text}>vro</Text>
          <Image source={require('../../assets/Images/wheel.png')} style={styles.logo} />
          <Text style={styles.text}>m</Text>
        </View>

        <TouchableOpacity style={styles.closeBtn} onPress={handleMenuClose}>
          <Image source={require('../../assets/Images/close2.png')} style={styles.close} />
        </TouchableOpacity>
      </View>

      <View style={styles.profileCon}>
        {profileImage ? (
          <Image source={{ uri: profileImage }} style={styles.profile} />
        ) : (
          <View style={styles.profile}></View>
        )}
        <Text style={styles.name}>{registeredName || ''}</Text>
      </View>

      <View style={styles.lineCon}>
        <View style={styles.VerticalLine} />
      </View>

      <View style={styles.contentCon}>
        <View style={styles.content}>
          <View style={styles.viewProfile}>
            <Image source={require('../../assets/Images/user2.png')} style={styles.icon} />
            <TouchableOpacity onPress={handleViewProfile}>
              <Text style={styles.contentText}>View profile</Text>
            </TouchableOpacity>
          </View>

          <View style={styles.settings}>
            <Image source={require('../../assets/Images/settings.png')} style={styles.icon} />
            <TouchableOpacity onPress={handleSetting}>
              <Text style={styles.contentText}>Settings</Text>
            </TouchableOpacity>
          </View>

          <View style={styles.help}>
            <Image source={require('../../assets/Images/question.png')} style={styles.icon} />
            <TouchableOpacity>
              <Text style={styles.contentText}>Help and feedback</Text>
            </TouchableOpacity>
          </View>

          <TouchableOpacity style={styles.shut}>
            <Image source={require('../../assets/Images/shut.png')} style={styles.icon} />
            <Text style={styles.contentText2}>Shut off</Text>
          </TouchableOpacity>
        </View>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
  },
  above: {
    justifyContent: 'space-between',
    alignItems: 'flex-start',
    flexDirection: 'row',
    marginTop: 40,
  },
  logoname: {
    marginLeft: 30,
    flexDirection: 'row',
    alignItems: 'center',
  },
  text: {
    fontSize: 32,
    fontFamily: 'BalooChettan2-ExtraBold',
    color: '#67A1FF',
  },
  logo: {
    width: 20,
    height: 20,
    marginTop: 7,
    marginLeft: -2,
  },
  closeBtn: {
    paddingTop: 20,
    marginRight: 20,
  },
  close: {
    width: 18,
    height: 18,
  },
  profileCon: {
    marginTop: 30,
    marginLeft: 25,
    flexDirection: 'row',
    alignItems: 'center',
  },
  profile: {
    width: 97,
    height: 93,
    backgroundColor: '#FFFCFC',
    borderWidth: 1,
    borderColor: '#BBBBBB',
    borderRadius: 100,
  },
  name: {
    fontSize: 18,
    fontFamily: 'Poppins-Bold',
    paddingLeft: 20,
  },
  VerticalLine: {
    flex: 1,
    height: 2,
    backgroundColor: '#C8C8C8',
    marginLeft: 10,
    marginRight: 10,
  },
  lineCon: {
    flexDirection: 'row',
    alignItems: 'center',
    marginVertical: 20,
    paddingLeft: 15,
    paddingRight: 15,
  },
  contentCon: {},
  content: {
    marginLeft: 35,
  },
  contentText: {
    fontSize: 15,
    fontFamily: 'Poppins-Medium',
    paddingLeft: 10,
  },
  contentText2: {
    fontSize: 15,
    fontFamily: 'Poppins-Medium',
    paddingLeft: 10,
    color: '#fff',
  },
  icon: {
    width: 20,
    height: 20,
  },
  viewProfile: {
    flexDirection: 'row',
    paddingLeft: 8,
    marginBottom: 10,
  },
  settings: {
    flexDirection: 'row',
    paddingLeft: 8,
    marginBottom: 10,
  },
  help: {
    flexDirection: 'row',
    paddingLeft: 8,
    marginBottom: 10,
  },
  shut: {
    paddingLeft: 8,
    width: 337,
    height: 40,
    flexDirection: 'row',
    backgroundColor: '#67A1FF',
    alignItems: 'center',
    borderRadius: 10,
  },
});

export default Menu;
