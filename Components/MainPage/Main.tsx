import React, { useEffect, useRef, useState } from 'react';
import { Dimensions, Image, Keyboard, KeyboardAvoidingView, StyleSheet, Text, TouchableOpacity, View } from 'react-native';
import { useNavigation } from '@react-navigation/native';
import { useFonts } from 'expo-font';
import { GooglePlaceDetail, GooglePlacesAutocomplete } from 'react-native-google-places-autocomplete';
import MapView, { LatLng, Marker, PROVIDER_GOOGLE } from 'react-native-maps';
import MapViewDirections from 'react-native-maps-directions';
import { GOOGLE_API_KEY } from '../../Environment/environment';


const { width, height } = Dimensions.get("window");

const ASPECT_RATIO = width / height;
const LATITUDE_DELTA = 0.02;
const LONGITUDE_DELTA = LATITUDE_DELTA * ASPECT_RATIO;
const INITIAL_POSITION = {
  latitude: 17.6131,
  longitude: 121.7220,
  latitudeDelta: LATITUDE_DELTA,
  longitudeDelta: LONGITUDE_DELTA,
};

type InputAutocompleteProps = {
  placeholder: string;
  onPlaceSelected: (details: GooglePlaceDetail | null) => void;
};

function InputAutocomplete({
  placeholder,
  onPlaceSelected,
}: InputAutocompleteProps) {
  return (
    <>
      <GooglePlacesAutocomplete
        styles={{ textInput: styles.input }}
        placeholder={placeholder || ""}
        fetchDetails
        onPress={(data, details = null) => {
          onPlaceSelected(details);
        }}
        query={{
          key: GOOGLE_API_KEY, // Remove the query key property
          language: "en",
        }}
      />
    </>
  );
}

function RouteInfoPopup({ distance, duration, onClose }) {
  return (
    <View style={styles.popupContainer}>
      <Text style={styles.popupText}>
        Distance: {distance.toFixed(2)} km
      </Text>
      <Text style={styles.popupText}>
        Duration: {Math.ceil(duration)} min
      </Text>
    </View>
  );
}
export default function Main() {
  console.log("API key:", GOOGLE_API_KEY);

  const navigation = useNavigation();

  const handleMenuPress = () => {
    navigation.navigate('menu' as never);
  };

  const [fontsLoaded] = useFonts({
    'BalooChettan2-ExtraBold': require('../../assets/Fonts/BalooChettan2-ExtraBold.ttf'),
    'Poppins-Medium': require('../../assets/Fonts/Poppins-Medium.ttf'),
    'Poppins-SemiBold': require('../../assets/Fonts/Poppins-SemiBold.ttf'),
    'Poppins-Bold': require('../../assets/Fonts/Poppins-Bold.ttf'),
  });

  useEffect(() => {
    // useEffect logic
  }, [fontsLoaded]);

  const [origin, setOrigin] = useState<LatLng | null>();
  const [destination, setDestination] = useState<LatLng | null>();
  const [showDirections, setShowDirections] = useState(false);
  const [distance, setDistance] = useState(0);
  const [duration, setDuration] = useState(0);
  const mapRef = useRef<MapView>(null);
  const [isSearchExpanded, setIsSearchExpanded] = useState(false);
  const [closeButtonOpacity, setCloseButtonOpacity] = useState(0);
  const [searchText, setSearchText] = useState('');
  const [popupVisible, setPopupVisible] = useState(false);
  const [isVehicleContainerExpanded, setIsVehicleContainerExpanded] = useState(false);
  const [isBusCon2Clicked, setIsBusCon2Clicked] = useState(false);



  const toggleVehicleContainer = () => {
    setIsVehicleContainerExpanded(!isVehicleContainerExpanded);
  };

  useEffect(() => {
    const keyboardDidShowListener = Keyboard.addListener('keyboardDidShow', () => {
      console.log('Keyboard did show');
      setIsSearchExpanded(true);

    });
    const keyboardDidHideListener = Keyboard.addListener('keyboardDidHide', () => {
      console.log('Keyboard did hide');
      if (isSearchExpanded) {
        // Collapse the search container only if it's currently expanded
        setIsSearchExpanded(false);
      }
    });

  return () => {
    keyboardDidShowListener.remove();
    keyboardDidHideListener.remove();
  };
}, []);

  const closeExpandedContainer = () => {
    setIsVehicleContainerExpanded(false);
    setIsBusCon2Clicked(false);
  };
  

  useEffect(() => {
    setCloseButtonOpacity(isSearchExpanded ? 1 : 0);
  }, [isSearchExpanded]);

  const closeSearchContainer = () => {
    setSearchText('');
    setIsSearchExpanded(false);
  };

  const moveTo = async (position: LatLng) => {
    const camera = await mapRef.current?.getCamera();
    if (camera) {
      camera.center = position;
      mapRef.current?.animateCamera(camera, { duration: 1000 });
    }
  };

  const edgePaddingValue = 70;

  const edgePadding = {
    top: edgePaddingValue,
    right: edgePaddingValue,
    bottom: edgePaddingValue,
    left: edgePaddingValue,
  };

  const traceRouteOnReady = (args: any) => {
    if (args) {
      setDistance(args.distance);
      setDuration(args.duration);
      setPopupVisible(true);
    }
  };

  const closePopup = () => {
    setPopupVisible(false);
  };

  const traceRoute = () => {
    if (origin && destination) {
      setShowDirections(true);
      mapRef.current?.fitToCoordinates([origin, destination], { edgePadding });
      Keyboard.dismiss();
      setIsSearchExpanded(false);
    }
  };


  const onPlaceSelected = (
    details: GooglePlaceDetail | null,
    flag: "origin" | "destination"
  ) => {
    const position = {
      latitude: details?.geometry.location.lat || 0,
      longitude: details?.geometry.location.lng || 0,
    };

    if (flag === "origin") {
      setOrigin(position);

      // Clear the destination when setting the origin
      if (!details) {
        setDestination(null);
      }
    } else {
      setDestination(position);
    }

    moveTo(position);
  };

  return (
    <View style={styles.container}>
      <MapView
        ref={mapRef}
        style={styles.map}
        provider={PROVIDER_GOOGLE}
        initialRegion={{
          latitude: origin?.latitude || INITIAL_POSITION.latitude,
          longitude: origin?.longitude || INITIAL_POSITION.longitude,
          latitudeDelta: LATITUDE_DELTA,
          longitudeDelta: LONGITUDE_DELTA,
        }}
      >
        {origin && <Marker coordinate={origin} />}
        {destination && <Marker coordinate={destination} />}
        {showDirections && origin && destination && (
          <MapViewDirections
            origin={origin}
            destination={destination}
            apikey={GOOGLE_API_KEY}
            strokeColor="#6644ff"
            strokeWidth={4}
            onReady={traceRouteOnReady}
          />
        )}
      </MapView>

      {popupVisible && (
        <RouteInfoPopup
          distance={distance}
          duration={duration}
          onClose={closePopup}
        />
      )}

      <TouchableOpacity style={styles.menu} onPress={handleMenuPress}>
        <Image source={require('../../assets/Images/burger.png')} style={styles.burger} />
      </TouchableOpacity>

      <KeyboardAvoidingView
        behavior="padding"
        style={[styles.searchContainer, { zIndex: 1, height: isSearchExpanded ? '95%' : 100 }]}
      >
        <View style={styles.searchCon2}>
          <View style={styles.inputContainer}>
            <Image source={require('../../assets/Images/search.png')} style={styles.icon1} />
            <View style={styles.inputAutoCon1}>
              <InputAutocomplete
                placeholder="Where to go?"
                onPlaceSelected={(details) => {
                  onPlaceSelected(details, "origin");
                }}
              />
            </View>
          </View>

          {origin && (
            <View>
              <View style={styles.inputContainer}>
                <Image source={require('../../assets/Images/search.png')} style={styles.icon2} />
                <View style={styles.inputAutoCon2}>
                  <InputAutocomplete
                    placeholder="Destination"
                    onPlaceSelected={(details) => {
                      onPlaceSelected(details, "destination");
                    }}
                  />
                </View>

                <TouchableOpacity style={styles.Tracebutton} onPress={traceRoute}>
                  <Text style={styles.buttonText}>Trace route</Text>
                </TouchableOpacity>
              </View>
            </View>
          )}
          
        </View>

        <View style={styles.updownCon}>
          <View style={styles.updownBtn}></View>
        </View>

        <TouchableOpacity
          style={[
            styles.closeButton,
            { opacity: closeButtonOpacity, top: isSearchExpanded ? 10 : 0 },
          ]}
          onPress={closeSearchContainer}
        >
          <Image source={require('../../assets/Images/close.png')} style={styles.close} />
        </TouchableOpacity>
      </KeyboardAvoidingView>

      {origin && showDirections && (
        <TouchableOpacity style={styles.vehicleCon} onPress={toggleVehicleContainer}>
          <Text style={styles.vehicleText}>View vehicle nearby</Text>
          <Image source={require('../../assets/Images/down.png')} style={styles.downIcon} />
        </TouchableOpacity>
      )}

      {isVehicleContainerExpanded && (
        <View style={styles.expandedContainer}>

          <TouchableOpacity style={styles.backBtn} onPress={closeExpandedContainer}>
            <Image source={require('../../assets/Images/left.png')} style={styles.leftIcon} />
          </TouchableOpacity>

            <Text style={styles.vehicleTypetext}>VEHICLE NEARBY YOU</Text>
            <View style={styles.busCon}>
              <Image source={require('../../assets/Images/bus1.png')} style={styles.vehicle} />
            </View>

          <View style={styles.vehicleInfoCon}>
            
            <View style={styles.vehicleInfo1}>
              <Image source={require('../../assets/Images/bus.png')} style={styles.icon} />
              <Text style={styles.Type}>Vehicle Type</Text>
              <Text style={styles.underType}>Donsal</Text>
            </View>
            
            <View style={styles.vehicleInfo2}>
              <Image source={require('../../assets/Images/arrival.png')} style={styles.icon} />
              <Text style={styles.arrive}>Arrival Time</Text>
              <Text style={styles.underArrive}>{Math.ceil(duration)} min</Text>
            </View>
            
            <View style={styles.vehicleInfo3}>
              <Image source={require('../../assets/Images/plate.png')} style={styles.icon} />
              <Text style={styles.plate}>Arrival Time</Text>
              <Text style={styles.underPlate}>N3G 5469</Text>
            </View>
            
            <TouchableOpacity style={styles.busCon2}></TouchableOpacity>
          </View>

            <TouchableOpacity style={styles.okBtn} onPress={closeExpandedContainer}>
            <Text style={styles.okText}>GOT IT</Text>
          </TouchableOpacity>
        </View>
      )}
    </View>
  );
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
    alignItems: "center",
    justifyContent: "center",
  },
  map: {
    width: Dimensions.get('window').width,
    height: '100%',
  },

  searchContainer: {
    width: '100%',
    alignItems: 'center',
    position: 'absolute',
    bottom: 0,
    backgroundColor: '#fff',
    borderTopLeftRadius: 30,
    borderTopRightRadius: 30,
    borderWidth: 1,
    borderColor: '#615F5F',


  },

  searchCon2: {
    width: '100%',
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 15,
  },

  inputContainer: {
    width: 375,
    height: 49,
  },

  input: {
    height: 49,
    backgroundColor: '#F0F0F0',
    borderRadius: 25,
    paddingLeft: 35,
    alignItems: 'center',
    justifyContent: 'space-between',
    fontFamily: 'Poppins-Medium',
    flexDirection: 'row',
  },

  inputAutoCon1: {
    height: 310,
  },

  inputAutoCon2: {
    height: 310,
    marginTop: 20,

  },

  icon1: {
    position: 'relative',
    top: 33,
    left: 13,
    width: 18,
    height: 18,
    zIndex: 2,
  },

  icon2: {
    position: 'relative',
    top: 53,
    left: 13,
    width: 18,
    height: 18,
    zIndex: 2,
  },


  Tracebutton: {
    height: 49,
    backgroundColor: "#67A1FF",
    paddingVertical: 12,
    marginTop: 50,
    borderRadius: 25,
  },
  buttonText: {
    textAlign: "center",
    fontFamily: 'Poppins-Medium',
    color: '#fff'
  },

  menu: {
    backgroundColor: '#fff',
    width: 56,
    height: 56,
    position: 'absolute',
    top: 50,
    left: 20,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 10,
    borderWidth: 1,
    borderColor: 'rgba(209, 207, 207, 0.5)',
    shadowColor: 'rgba(0, 0, 0, .2)',
    shadowOffset: { width: 0, height: 1 },
    shadowOpacity: 0.8,
    shadowRadius: 4,
    elevation: 5,
  },
  burger: {
    width: 30,
    height: 30,
  },

  updownCon: {
    position: 'absolute',
    top: 10,
    paddingBottom: 18,
  },

  updownBtn: {
    width: 75,
    height: 10,
    backgroundColor: '#D9D9D9',
    borderRadius: 10,
  },

  closeButton: {
    position: 'absolute',
    top: 10,
    right: 20,
    width: 20,
    height: 20,
    elevation: 10,
  },

  close: {
    width: 20,
    height: 20,
  },

  popupContainer: {
    position: 'absolute',
    top: 40,
    width: '50%',
    backgroundColor: '#67A1FF',
    borderRadius: 10,
    padding: 10,
    alignItems: 'center',
    alignSelf: 'center',
  },

  popupText: {
    fontFamily: 'Poppins-Medium',
    marginBottom: 5,
    color: '#fff'
  },

  vehicleCon: {
    justifyContent: 'space-evenly',
    alignItems: 'center',
    flexDirection: 'row',
    width: 196,
    height: 31,
    backgroundColor: '#67A1FF',
    position: 'absolute',
    bottom: 110,
    borderRadius: 20,
  },

  vehicleText: {
    textAlign: "center",
    color: '#fff',
    fontFamily: 'Poppins-SemiBold',
    fontSize: 14,
  },

  downIcon: {
    width: 15,
    height: 15,
  },

  expandedContainer: {
    zIndex: 1,
    position: 'absolute',
    height: "80%",
    bottom: 0,
    backgroundColor: '#fff',
    width: '100%',
    padding: 20,
    alignItems: 'center',
    justifyContent: 'center',
  },

  backBtn:{
    position: 'absolute',
    left: 15,
    top: 15,
  },

  leftIcon:{
    height: 18,
    width: 18,
  },

 
  busCon: {
    width: 255,
    height: 245,
    backgroundColor: '#67A1FF',
    shadowColor: "black",
    shadowOffset: { width: 2, height: 2 },
    shadowOpacity: 0.5,
    shadowRadius: 4,
    elevation: 4,
    padding: 8,
    borderRadius: 15,
    justifyContent: 'center',
    alignItems: 'center',
  },

  busCon2: {
    position: 'absolute',
    bottom: 295,
    right: 0,
    width: 33,
    height: 245,
    backgroundColor: '#67A1FF',
    shadowColor: "black",
    shadowOffset: { width: 2, height: 2 },
    shadowOpacity: 0.5,
    shadowRadius: 4,
    elevation: 4,
    padding: 8,
    borderTopLeftRadius: 15,
    borderBottomLeftRadius: 15,
    justifyContent: 'center',
    alignItems: 'center',
  },

  vehicleTypetext: {
    fontFamily: 'Poppins-Bold',
    fontSize: 20,
  },

  vehicle: {
    width: 167,
    height: 167,
  },

  vehicleInfoCon: {
    marginTop: 20,
  },

  vehicleInfo1: {
    width: 410,
    height: 82,
    backgroundColor: '#F5FEFF',
    borderTopWidth: 1,
    borderBottomWidth: 1,
    borderColor: '#000',
    marginBottom: 10,
    justifyContent: 'center',
    paddingLeft: 100,

  },

  vehicleInfo2: {
    width: 410,
    height: 82,
    backgroundColor: '#EEF4FF',
    borderTopWidth: 1,
    borderBottomWidth: 1,
    borderColor: '#000',
    marginBottom: 10,
    justifyContent: 'center',
    paddingLeft: 100,

  },

  vehicleInfo3: {
    width: 410,
    height: 82,
    backgroundColor: '#FFF5F5',
    borderTopWidth: 1,
    borderBottomWidth: 1,
    borderColor: '#000',
    marginBottom: 10,
    justifyContent: 'center',
    paddingLeft: 100,
  },

  okBtn: {
    width: 337,
    height: 49,
    backgroundColor: '#67A1FF',
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 20,
    marginTop: 5,
  },

  okText: {
    fontFamily: 'Poppins-Bold',
    color: '#fff',
    fontSize: 16,
  },

  icon: {
    width: 30,
    height: 30,
    position: 'absolute',
    top: 25,
    left: 55,
  },

  Type: {
    fontFamily: 'Poppins-Bold',
    fontSize: 16,

  },

  underType: {
    fontFamily: 'Poppins-Medium',
    fontSize: 14,

  },

  arrive: {
    fontFamily: 'Poppins-Bold',
    fontSize: 16,

  },

  underArrive: {
    fontFamily: 'Poppins-Medium',
    fontSize: 14,
  },

  plate: {
    fontFamily: 'Poppins-Bold',
    fontSize: 16,

  },

  underPlate: {
    fontFamily: 'Poppins-Medium',
    fontSize: 14,

  },
});