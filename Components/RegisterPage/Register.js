import React, { useState, useEffect } from 'react';
import { View, Text, Image, TouchableOpacity, ScrollView } from 'react-native';
import { Input } from 'react-native-elements';
import { useFonts } from 'expo-font';
import { useNavigation } from '@react-navigation/native';
import { RadioButton } from 'react-native-paper';
import PasswordStrengthChecker from '../../PasswordIndicator/PasswordStrengthChecker';
import { useDispatch, useSelector } from 'react-redux';
import { register, login } from '../../Actions/authActions';

const Register = () => {
  const navigation = useNavigation();
  const dispatch = useDispatch();
  const authState = useSelector((state) => state.auth);

  const [fontsLoaded] = useFonts({
    'BalooChettan2-ExtraBold': require('../../assets/Fonts/BalooChettan2-ExtraBold.ttf'),
    'Poppins-Medium': require('../../assets/Fonts/Poppins-Medium.ttf'),
    'Poppins-SemiBold': require('../../assets/Fonts/Poppins-SemiBold.ttf'),
  });

  const [password, setPassword] = useState('');
  const [address, setAddress] = useState('');
  const [confirmPassword, setConfirmPassword] = useState('');
  const [userType, setUserType] = useState('');
  const [operatorType, setOperatorType] = useState('');
  const [vehicleType, setVehicleType] = useState('');
  const [name, setName] = useState('');
  const [email, setEmail] = useState('');

  const resetFields = () => {
    setName('');
    setEmail('');
    setPassword('');
    setConfirmPassword('');
    setAddress('');
    setUserType('');
    setOperatorType('');
    setVehicleType('');
  };

  useEffect(() => {
    if (authState.user) {
      resetFields();
    }
  }, [authState.user]);

  const handleContinuePress = async () => {
    const existingUser = authState.users && authState.users[email];
  
    if (existingUser) {
      console.log('Email already exists. Please choose a different email.');
      return;
    }
  
    const userData = {
      name,
      email,
      password,
      address,
      userType,
      operatorType,
      vehicleType,
    };
  
    try {
      // Dispatch the registration action
      await dispatch(register(userData));
      console.log('Registration action dispatched');
    } catch (error) {
      console.error('Error during registration:', error);
      return;
    }
  
    // If registration was successful, you can proceed with login and navigation
    // ...
  
    // Check if registration was successful before proceeding
    if (authState.user) {
      // Dispatch the login action after registration
      dispatch(login({ email, password }));
      console.log('Login action dispatched');
  
      // Navigate to the 'main' screen after successful registration and login
      navigation.navigate('main');
      console.log('Navigated to main');
  
      // Reset fields after successful registration and login
      resetFields();
    }
  };
  

  return (
    <ScrollView contentContainerStyle={styles.scrollContainer}>
      <View style={styles.container}>

        <TouchableOpacity style={styles.backBtn} onPress={() => navigation.navigate('login')}>
          <Image source={require('../../assets/Images/left.png')} style={styles.backImg} />
        </TouchableOpacity>

        <View style={styles.content}>
          <Text style={styles.text1}>vro</Text>
          <Image source={require('../../assets/Images/wheel.png')} style={styles.logo} />
          <Text style={styles.text1}>m</Text>
        </View>
        <View style={styles.textUnder}>
          <Text style={styles.text2}>Register now!</Text>
        </View>

        <Input
          placeholder="Name"
          leftIcon={<Image source={require('../../assets/Images/user.png')} style={styles.Icon} />}
          onChangeText={(text) => setName(text)}
          inputContainerStyle={{ borderBottomWidth: 0 }}
          containerStyle={styles.input}
          inputStyle={{ fontSize: 16, color: 'black', fontFamily: 'Poppins-Medium', paddingTop: 7 }}
        />

        <Input
          placeholder="Email"
          leftIcon={<Image source={require('../../assets/Images/email.png')} style={styles.Icon} />}
          onChangeText={(text) => setEmail(text)}
          inputContainerStyle={{ borderBottomWidth: 0 }}
          containerStyle={styles.input}
          inputStyle={{ fontSize: 16, color: 'black', fontFamily: 'Poppins-Medium', paddingTop: 7 }}
        />

        <Input
          placeholder="Address"
          leftIcon={<Image source={require('../../assets/Images/location.png')} style={styles.Icon} />}
          onChangeText={(text) => setAddress(text)}
          inputContainerStyle={{ borderBottomWidth: 0 }}
          containerStyle={[styles.input, styles.addressInput]}
          inputStyle={{ fontSize: 16, color: 'black', fontFamily: 'Poppins-Medium', paddingTop: 7 }}
        />

        <Input
          placeholder="Password"
          leftIcon={<Image source={require('../../assets/Images/lock.png')} style={styles.Icon} />}
          onChangeText={(text) => setPassword(text)}
          inputContainerStyle={{ borderBottomWidth: 0 }}
          containerStyle={[styles.input, styles.passwordInput]}
          inputStyle={{ fontSize: 16, color: 'black', fontFamily: 'Poppins-Medium', paddingTop: 7 }}
          secureTextEntry
        />
        <PasswordStrengthChecker password={password} />

        <Input
          placeholder="Confirm Password"
          leftIcon={<Image source={require('../../assets/Images/lock.png')} style={styles.Icon} />}
          onChangeText={(text) => setConfirmPassword(text)}
          inputContainerStyle={{ borderBottomWidth: 0 }}
          containerStyle={[styles.input, styles.passwordInput]}
          inputStyle={{ fontSize: 16, color: 'black', fontFamily: 'Poppins-Medium', paddingTop: 7 }}
          secureTextEntry
        />
        <PasswordStrengthChecker password={confirmPassword} />

        <View style={styles.radioContainer}>
          <Text style={styles.text4}>User type</Text>
          <View style={styles.radioGroup}>
            <View style={styles.radioItem}>
              <RadioButton
                value="commuter"
                status={userType === 'commuter' ? 'checked' : 'unchecked'}
                onPress={() => {
                  setUserType('commuter');
                  setOperatorType(null);
                }}
                color="#67A1FF"
              />
              <Text style={styles.radioLabel}>Commuter</Text>
            </View>

            <View style={styles.radioItem}>
              <RadioButton
                value="operator"
                status={userType === 'operator' ? 'checked' : 'unchecked'}
                onPress={() => setUserType('operator')}
                color="#67A1FF"
              />
              <Text style={styles.radioLabel}>Operator</Text>
            </View>

            {userType === 'operator' && (
              <>
                <Text style={styles.text4}>Operator type</Text>
                <View style={styles.radioItem}>
                  <RadioButton
                    value="driver"
                    status={operatorType === 'driver' ? 'checked' : 'unchecked'}
                    onPress={() => {
                      setOperatorType('driver');
                      setVehicleType(null);
                    }}
                    color="#67A1FF"
                  />
                  <Text style={styles.radioLabel}>Driver</Text>
                </View>

                <View style={styles.radioItem}>
                  <RadioButton
                    value="conductor"
                    status={operatorType === 'conductor' ? 'checked' : 'unchecked'}
                    onPress={() => {
                      setOperatorType('conductor');
                      setVehicleType(null);
                    }}
                    color="#67A1FF"
                  />
                  <Text style={styles.radioLabel}>Conductor</Text>
                </View>

                {operatorType && (
                  <>
                    <Text style={styles.text4}>Vehicle type</Text>
                    <View style={styles.radioItem}>
                      <RadioButton
                        value="donsal"
                        status={vehicleType === 'donsal' ? 'checked' : 'unchecked'}
                        onPress={() => setVehicleType('donsal')}
                        color="#67A1FF"
                      />
                      <Text style={styles.radioLabel}>Donsal</Text>
                    </View>

                    <View style={styles.radioItem}>
                      <RadioButton
                        value="puv"
                        status={vehicleType === 'puv' ? 'checked' : 'unchecked'}
                        onPress={() => setVehicleType('puv')}
                        color="#67A1FF"
                      />
                      <Text style={styles.radioLabel}>PUV</Text>
                    </View>   
                  </>
                )}
              </>
            )}
          </View>
        </View>

        <TouchableOpacity style={styles.Btn} onPress={handleContinuePress}>
          <Text style={styles.btnText}>Register</Text>
        </TouchableOpacity>
      </View>
    </ScrollView>
  );
};
const styles = ({
  scrollContainer: {
    flexGrow: 1,
  },
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#fff',
},

backBtn: {
  position: "absolute",
  top: 55,
  left: 30,
},
backImg:{
    width: 20,
    height: 20,
},
  content: {
    flexDirection: 'row',
    alignItems: 'center',
},
  text1: {
    fontSize: 64,
    fontFamily: 'BalooChettan2-ExtraBold',
    color: '#67A1FF',
},
  text2: {
    fontSize: 16,
    fontFamily: 'Poppins-Medium',
    marginBottom: 60,
},
  text3: {
  fontSize: 16,
  fontFamily: 'Poppins-Medium',
  marginBottom: 10,
},
  logo: {
    width: 42,
    height: 42,
    marginTop: 14,
    marginLeft: -4,
},
  input: {
    width: 350,
    height: 55,
    backgroundColor: '#fff',
    borderWidth: 1,
    borderColor: '#000',
    borderRadius: 15,
    paddingHorizontal: 10,
    shadowColor: 'black',
    marginBottom: 15,
},
  Icon: {
    width: 18,
    height: 18,
    marginTop: 5,
},
Btn:{
  backgroundColor: '#67A1FF',
  width: 317,
  height: 55,
  alignItems: 'center',
  justifyContent: 'center',
  borderRadius: 25,
  marginTop: 50,
  position: 'relative',
  top: -10,
},
btnText:{
  fontFamily: 'Poppins-SemiBold',
  fontSize: 16,
  color: '#fff',
},

text4: {
  fontSize: 16,
  fontFamily: 'Poppins-Medium',
  marginBottom: 10,
  marginTop: 5,

},
radioContainer: {
  width: "80%",
  alignItems: 'flex-start', 
},
radioGroup: {
  flexDirection: 'column',
},
radioItem: {
  flexDirection: 'row',
  alignItems: 'center',
  marginRight: 20,
},
radioLabel: {
  fontSize: 16,
  marginLeft: 8,
  fontFamily: 'Poppins-Medium',
},
});


export default Register;