import React from 'react';
import { View, Text, Image, StyleSheet, TouchableOpacity, ScrollView } from 'react-native';
import { useFonts } from 'expo-font';
import { useNavigation } from '@react-navigation/native';

const Setting = () => {
  const navigation = useNavigation();

  const [fontsLoaded] = useFonts({
    'BalooChettan2-ExtraBold': require('../../assets/Fonts/BalooChettan2-ExtraBold.ttf'),
    'Poppins-Medium': require('../../assets/Fonts/Poppins-Medium.ttf'),
    'Poppins-SemiBold': require('../../assets/Fonts/Poppins-SemiBold.ttf'),
    'Poppins-Bold': require('../../assets/Fonts/Poppins-Bold.ttf'),
  });

  if (!fontsLoaded) {
    return null;
  }

  const handleMenuClose = () => {
    navigation.navigate('main');
  };

  const handleMenuBack = () => {
    navigation.goBack();
  };

  return (
    <View style={styles.container}>
      <View style={styles.above}>
        <TouchableOpacity style={styles.closeBtn} onPress={handleMenuBack}>
          <Image source={require('../../assets/Images/left.png')} style={styles.close} />
        </TouchableOpacity>

        <View style={styles.logoname}>
          <Text style={styles.text}>vro</Text>
          <Image source={require('../../assets/Images/wheel.png')} style={styles.logo} />
          <Text style={styles.text}>m</Text>
        </View>

        <TouchableOpacity style={styles.closeBtn} onPress={handleMenuClose}>
          <Image source={require('../../assets/Images/close2.png')} style={styles.close} />
        </TouchableOpacity>
      </View>

      <ScrollView style={styles.scrollContainer}>
        <View style={styles.content}>
          <View style={styles.profile}></View>
          <TouchableOpacity style={styles.editBtn}>
            <Image source={require('../../assets/Images/pencil.png')} style={styles.editIcon} />
          </TouchableOpacity>
        </View>

        <View style={styles.advanceCon}>
          <Text style={styles.advanceText}>Login info</Text>
          <View style={styles.advanceContent}>
            <View style={styles.details}>
              <TouchableOpacity>
                <Text style={styles.advanceText1}>Logout</Text>
              </TouchableOpacity>
            </View>

            <View style={styles.lineCon}>
              <View style={styles.VerticalLine} />
            </View>

            <View style={styles.details}>
              <TouchableOpacity>
                <Text style={styles.advanceText2}>Switch account</Text>
              </TouchableOpacity>
            </View>

            <View style={styles.lineCon}>
              <View style={styles.VerticalLine} />
            </View>

            <View style={styles.details}>
              <TouchableOpacity>
                <Text style={styles.advanceText3}>Delete account</Text>
              </TouchableOpacity>
            </View>
          </View>
        </View>
      </ScrollView>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
  },
  scrollContainer: {
    flexGrow: 1,
  },
  above: {
    justifyContent: 'space-between',
    alignItems: 'flex-start',
    flexDirection: 'row',
    marginTop: 40,
  },
  logoname: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  text: {
    fontSize: 32,
    fontFamily: 'BalooChettan2-ExtraBold',
    color: '#67A1FF',
  },
  logo: {
    width: 20,
    height: 20,
    marginTop: 7,
    marginLeft: -2,
  },
  closeBtn: {
    paddingTop: 20,
    marginRight: 20,
    marginLeft: 20,
  },
  close: {
    width: 18,
    height: 18,
  },
  content: {
    width: '100%',
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 20,
    marginTop: 25,
  },
  profile: {
    width: 97,
    height: 93,
    backgroundColor: '#FFFCFC',
    borderWidth: 1,
    borderColor: '#BBBBBB',
    borderRadius: 100,
  },
  editIcon: {
    width: 13,
    height: 13,
  },
  editBtn: {
    position: 'absolute',
    bottom: -1,
    right: 165,
    width: 25,
    height: 25,
    backgroundColor: '#67A1FF',
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 50,
  },
  detailsCon: {
    marginTop: 40,
  },
  advanceText1: {
    fontSize: 16,
    fontFamily: 'Poppins-Bold',
    color: '#fff',
    paddingTop: 10,
  },
  advanceText2: {
    fontSize: 16,
    fontFamily: 'Poppins-Bold',
    color: '#fff',
    paddingTop: 10,
  },
  advanceText3: {
    fontSize: 16,
    fontFamily: 'Poppins-Bold',
    color: '#C86B6B',
  },
  VerticalLine: {
    flex: 1,
    height: 2,
    backgroundColor: '#fff',
  },
});

export default Setting;
