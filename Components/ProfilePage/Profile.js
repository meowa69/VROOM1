import React from 'react';
import { View, Text, Image, StyleSheet, TouchableOpacity } from 'react-native';
import { useFonts } from 'expo-font';
import { useNavigation } from '@react-navigation/native';
import { useSelector, useDispatch } from 'react-redux';
import { logout } from '../../Actions/authActions';

const Profile = () => {
  const navigation = useNavigation();
  const registeredName = useSelector((state) => state.auth.registeredName);
  const dispatch = useDispatch();
  const profileImage = useSelector((state) => state.auth.profileImage);

  const handleLogout = () => {
    dispatch(logout());
    navigation.replace('login');
  };

  const [fontsLoaded] = useFonts({
    'BalooChettan2-ExtraBold': require('../../assets/Fonts/BalooChettan2-ExtraBold.ttf'),
    'Poppins-Medium': require('../../assets/Fonts/Poppins-Medium.ttf'),
    'Poppins-SemiBold': require('../../assets/Fonts/Poppins-SemiBold.ttf'),
    'Poppins-Bold': require('../../assets/Fonts/Poppins-Bold.ttf'),
  });

  if (!fontsLoaded) {
    return null;
  }

  const handleMenuBack = () => {
    navigation.goBack();
  };

  const handleMenuClose = () => {
    navigation.navigate('main');
  };

  const handleAccount = () => {
    navigation.navigate('account');
  };

  const handleSetting = () => {
    navigation.navigate('setting');
  };

  return (
    <View style={styles.container}>
      <View style={styles.above}>
        <TouchableOpacity style={styles.closeBtn} onPress={handleMenuBack}>
          <Image source={require('../../assets/Images/left.png')} style={styles.close} />
        </TouchableOpacity>

        <View style={styles.logoname}>
          <Text style={styles.text}>vro</Text>
          <Image source={require('../../assets/Images/wheel.png')} style={styles.logo} />
          <Text style={styles.text}>m</Text>
        </View>

        <TouchableOpacity style={styles.closeBtn} onPress={handleMenuClose}>
          <Image source={require('../../assets/Images/close2.png')} style={styles.close} />
        </TouchableOpacity>
      </View>

      <View style={styles.profileCon}>
        <View style={styles.content}>
          {profileImage ? (
            <Image source={{ uri: profileImage }} style={styles.profile} />
          ) : (
            <View style={styles.profile}></View>
          )}
          <Text style={styles.name}>{registeredName || ''}</Text>
        </View>
      </View>

      <View style={styles.content2Con}>
        <TouchableOpacity style={styles.account} onPress={handleAccount}>
          <Image source={require('../../assets/Images/profile.png')} style={styles.icon} />
          <Text style={styles.contentText}>Account and login</Text>
        </TouchableOpacity>

        <TouchableOpacity style={styles.settings} onPress={handleSetting}>
          <Image source={require('../../assets/Images/setting.png')} style={styles.icon} />
          <Text style={styles.contentText}>Settings</Text>
        </TouchableOpacity>

        <TouchableOpacity style={styles.logout} onPress={handleLogout}>
          <Image source={require('../../assets/Images/logout.png')} style={styles.icon} />
          <Text style={styles.contentText}>Logout</Text>
        </TouchableOpacity>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
  },
  above: {
    justifyContent: 'space-between',
    alignItems: 'flex-start',
    flexDirection: 'row',
    marginTop: 40,
  },
  logoname: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  text: {
    fontSize: 32,
    fontFamily: 'BalooChettan2-ExtraBold',
    color: '#67A1FF',
  },
  logo: {
    width: 20,
    height: 20,
    marginTop: 7,
    marginLeft: -2,
  },
  closeBtn: {
    paddingTop: 20,
    marginRight: 20,
    marginLeft: 20,
  },
  close: {
    width: 18,
    height: 18,
  },
  profileCon: {
    marginTop: 55,
    alignItems: 'center',
  },
  content: {
    width: 209,
    height: 128,
    backgroundColor: '#67A1FF',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 20,
  },
  profile: {
    position: 'absolute',
    top: -30,
    width: 97,
    height: 93,
    backgroundColor: '#FFFCFC',
    borderWidth: 1,
    borderColor: '#BBBBBB',
    borderRadius: 100,
  },
  name: {
    paddingTop: 50,
    fontSize: 18,
    fontFamily: 'Poppins-Bold',
    color: '#fff',
  },
  content2Con: {
    marginTop: 30,
  },
  account: {
    width: '100%',
    height: 69,
    backgroundColor: '#67A1FF',
    marginBottom: 10,
    flexDirection: 'row',
    alignItems: 'center',
    paddingLeft: 25,
  },
  settings: {
    width: '100%',
    height: 69,
    backgroundColor: '#67A1FF',
    marginBottom: 10,
    flexDirection: 'row',
    alignItems: 'center',
    paddingLeft: 25,
  },
  logout: {
    width: '100%',
    height: 69,
    backgroundColor: '#67A1FF',
    marginBottom: 10,
    flexDirection: 'row',
    alignItems: 'center',
    paddingLeft: 25,
  },
  contentText: {
    fontSize: 15,
    fontFamily: 'Poppins-SemiBold',
    paddingLeft: 10,
    color: '#fff',
  },
  icon: {
    width: 30,
    height: 30,
  },
});

export default Profile;