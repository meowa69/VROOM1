const initialState = {
  user: null,
  isAuthenticated: false,
  profileImage: null,
};

const authReducer = (state = initialState, action) => {
  switch (action.type) {
    case 'LOGIN':
      return {
        ...state,
        user: action.payload,
        isAuthenticated: true,
      };

      case 'REGISTER':
        return {
          ...state,
          users: { ...state.users, [action.payload.email]: action.payload },
          isAuthenticated: true,
          registeredName: action.payload.name, // Add this line
          registeredEmail: action.payload.email,
          registeredAddress: action.payload.address,
          registeredPassword: action.payload.password,
          registeredUserType: action.payload.userType,

        };

    case 'LOGOUT':
      return {
        ...state,
        user: null,
        isAuthenticated: false,
      };

      case 'SET_PROFILE_IMAGE':
      return {
        ...state,
        profileImage: action.payload,
      };

    default:
      return state;
  }
};

export default authReducer;